package Chapter_2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Этот аплет отображает приветствие авторов книги
 * @version 1.22 2007-04-08
 * @author Cay Horstman
 */
public class WelcomeApplet extends JApplet
{
    public void init()
    {
        EventQueue.invokeLater(() -> {
            setLayout(new BorderLayout());

            JLabel label =
                    new JLabel(getParameter("greeting"), SwingConstants.CENTER);
            label.setFont(new Font("Serif", Font.BOLD, 18));
            add(label, BorderLayout.CENTER);

            JPanel panel = new JPanel();

            JButton cayButton = new JButton("Cay Horstmann");
            cayButton.addActionListener(makeAction("http://www.horstman.com"));
            panel.add(cayButton);

            JButton garyButton = new JButton("Gary Cornell");
            garyButton.addActionListener(makeAction("mailto:gary_cornell@apress.com"));
            panel.add(garyButton);

            add(panel, BorderLayout.SOUTH);
        });
    }
    private ActionListener makeAction(final String urlString)
    {
        return event -> {
            try
            {
                getAppletContext().showDocument(new URL(urlString));
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
        };
    }
}

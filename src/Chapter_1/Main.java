package Chapter_1;

public class Main {
    public static void main(String[] args) {
        System.out.println("Характерные особенности языка:\n" +
                "- простой;\n" +
                "- объектно-ориентированный;\n" +
                "- распределенный;\n" +
                "- надежный;\n" +
                "- безопасный;\n" +
                "- не зависящий от архетиктуры;\n" +
                "- переносимый;\n" +
                "- интерпретируемый;\n" +
                "- высокопроизводительный;\n" +
                "- многопоточный;\n" +
                "- динамичный.\n" +
                "\n" +
                "Аплет-программа Java работающая под управлением интернет-браузера.\n" +
                "Пример: http://jmol.sourceforge.net");
    }
}

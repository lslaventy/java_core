package Chapter_3;


/**
 * Это первый пример программы в главе 3
 * @version 1.01 1997-03-22
 * @author Gary Cornell
 */

public class FirstSample {
    public static void main(String[] args) {
//        System.out.println("We will not use 'Hello, world!'");
//        System.out.printf("%f", 3.14D);
//        System.out.printf("\n %f", 3.14F);
//        double b = Double.POSITIVE_INFINITY;
//        if (Double.isInfinite(b)){
//            System.out.println("pozitive");
//        }
//        System.out.println('\uFFFF');
//        char c = '\u0000';
//int q = 0;
//        for (int i =0; i < 65536; i++){
//            while (q < 50) {
//                c = (char) (i++);
//                System.out.print(String.valueOf(i) + c + " ");
//                q++;
//            }
//            q=0;
//            System.out.println();

        final int df;
        df = 100;
        System.out.println(df);
        char pic = '\uEBA1';
        int count = 0;
        try {
            do {
                Thread.sleep(1);
                if (pic < '\uEBAA') {
                    System.out.print(pic);
                    System.out.print("\b");
                    System.out.print(" ");
                    System.out.print("\b");
                    pic = (char) (pic + (1));
                    } else {
                        pic = '\uEBA1';
                }
                count++;
                } while (count < 50);
            } catch (InterruptedException e) {
                e.printStackTrace();
        }
    }
}
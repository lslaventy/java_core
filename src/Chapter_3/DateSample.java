package Chapter_3;

import java.util.Date;

public class DateSample {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.printf("%tc\n", date);
        System.out.printf("%tF\n", date);
        System.out.printf("%tD\n", date);
        System.out.printf("%tT\n", date);
        System.out.printf("%tr\n", date);
        System.out.printf("%tR\n", date);
        System.out.printf("%tY\n", date);
        System.out.printf("%ty\n", date);
        System.out.printf("%tC\n", date);
        System.out.printf("%tB\n", date);
        System.out.printf("%tb\n", date);
        System.out.printf("%th\n", date);
        System.out.printf("%tm\n", date);
        System.out.printf("%td\n", date);
        System.out.printf("%te\n", date);
        System.out.printf("%tA\n", date);
        System.out.printf("%ta\n", date);
        System.out.printf("%tj\n", date);
        System.out.printf("%tH\n", date);
        System.out.printf("%tk\n", date);
        System.out.printf("%tI\n", date);
        System.out.printf("%tl\n", date);
        System.out.printf("%tM\n", date);
        System.out.printf("%tS\n", date);
        System.out.printf("%tL\n", date);
        System.out.printf("%tN\n", date);
//        System.out.printf("%tP\n", date);
        System.out.printf("%tp\n", date);
        System.out.printf("%tZ\n", date);
        System.out.printf("%tz\n", date);
        System.out.printf("%ts\n", date);
        System.out.printf("%tQ\n", date);
    }
}

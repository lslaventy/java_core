package Chapter_3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class SimpleInOutFile {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new File("print.txt"));
        pw.print("Привет_народ, пишу из printWriter");
        pw.close();

        Scanner in = new Scanner(new File("print.txt"));
            if (in.hasNext()){
                System.out.println(in.next());
            }
            in.close();
    }
}
